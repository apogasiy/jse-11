package main.java.com.tsc.apogasiy.tm.api.controller;

import main.java.com.tsc.apogasiy.tm.model.Task;

public interface ITaskController {

    void showTasks();

    void clearTasks();

    void createTask();

    void showTask(Task task);

    void showById();

    void showByName();

    void showByIndex();

    void updateByIndex();

    void updateById();

    void removeById();

    void removeByName();

    void removeByIndex();

}
