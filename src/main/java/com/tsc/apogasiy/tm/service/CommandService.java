package main.java.com.tsc.apogasiy.tm.service;

import main.java.com.tsc.apogasiy.tm.api.repository.ICommandRepository;
import main.java.com.tsc.apogasiy.tm.api.service.ICommandService;
import main.java.com.tsc.apogasiy.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getCommands() {
        return commandRepository.getCommands();
    }
}
